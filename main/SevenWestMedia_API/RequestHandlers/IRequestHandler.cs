﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace SevenWestMedia_API.RequestHandlers
{
    public interface IRequestHandler
    {
        string GetReleases(string url);
    }
}
