﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using SevenWestMedia_API.Constants;

namespace SevenWestMedia_API.RequestHandlers
{
    public class WebClientRequestHandler : IRequestHandler
    {
        public string GetReleases(string url)
        {
            var client = new WebClient();
            client.Headers.Add(RequestConstants.UserAgent, RequestConstants.UserAgentValue);

            var response = client.DownloadString(url);

            return response;
        }
    }
}
