﻿namespace SevenWestMedia_API.Constants
{
    class RequestConstants
    {
        public const string BaseUrl = "https://f43qgubfhf.execute-api.ap-southeast-2.amazonaws.com/";
        public const string Url = "https://f43qgubfhf.execute-api.ap-southeast-2.amazonaws.com/sampletest";
        public const string UserAgent = "User-Agent";
        public const string UserAgentValue = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";
    }
}
