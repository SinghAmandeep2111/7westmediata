﻿using System;
using System.Linq;
using System.Text;
using SevenWestMedia_API.Events;
namespace SevenWestMedia_API
{
    class Program
    {
        static void Main(string[] args)
        {
            //1st Question
            var GetDetails = new Event();
            Console.WriteLine("The users full name for id=42 : {0}", GetDetails.GetUserDetails(42));

            //2nd Question
            Console.WriteLine();
            var GetUsers = GetDetails.GetUsersByAge(23);
            StringBuilder sb = new StringBuilder();
            foreach (var Name in GetUsers)
            {
                sb.Append(Name.First + ",");
            }
            if (sb.Length > 0) sb.Length--;
            string FinalName = sb.ToString();
            Console.WriteLine("All the users first names (comma separated) who are 23 : {0}", FinalName);

            //3rd Question
            Console.WriteLine();
            Console.WriteLine("The number of genders per Age, displayed from youngest to oldest :");
            var GetUserAgeGroup = GetDetails.GetAllUsers().GroupBy(s => s.Age);

            foreach (var UsersInAgeGroup in GetUserAgeGroup)
            {
                Console.WriteLine("Age: " + UsersInAgeGroup.Key + " Female: " + UsersInAgeGroup.Count(s => s.Gender == "F") + " Male: " + UsersInAgeGroup.Count(s => s.Gender == "M").ToString());
            }

        }
    }
}
