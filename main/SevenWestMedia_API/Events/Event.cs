﻿using Newtonsoft.Json;
using SevenWestMedia_API.RequestHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SevenWestMedia_API.Model;
using System.Configuration;
namespace SevenWestMedia_API.Events
{
    class Event
    {
        IRequestHandler webClientRequestHandler = new WebClientRequestHandler();
        public string GetUserDetails(int UserID)
        {
            var response = GetReleases(webClientRequestHandler);
            var UserDetails = JsonConvert.DeserializeObject<List<UserDetails>>(response).FirstOrDefault(s => s.Id == UserID);
            var FullName = "User not found";
            if (UserDetails != null) FullName = UserDetails.First + ' ' + UserDetails.Last;
            return FullName;
        }

        public List<UserDetails> GetUsersByAge(int Age)
        {
            var response = GetReleases(webClientRequestHandler);
            var Users = JsonConvert.DeserializeObject<List<UserDetails>>(response).Where(s => s.Age == Age);
            var UserList = Users.ToList();
            return (List<UserDetails>)UserList;
        }

        public List<UserDetails> GetAllUsers()
        {
            var response = GetReleases(webClientRequestHandler);
            var Users = JsonConvert.DeserializeObject<List<UserDetails>>(response);
            var UserList = Users.ToList();
            return (List<UserDetails>)Users;
        }


        public static string GetReleases(IRequestHandler requestHandler)
        {
            return requestHandler.GetReleases(Constants.RequestConstants.Url);
        }
    }
}
